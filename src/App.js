import React from 'react';

import './App.scss'

import DragAndDrop from './pages/DragAndDrop/DragAndDrop'

function App() {
  return (
    <div className="App">
      <DragAndDrop/>
    </div>
  );
}

export default App;
