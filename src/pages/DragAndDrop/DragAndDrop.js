import React, { useState } from 'react'
import './DragAndDrop.scss'
import { ArrayHelper } from '../../shared'

const DragAndDrop = () => {

    // Listado de categorias
    const [categoriesState, setCategoriesState] = useState([
        {
            code: "CAT01",
            title: "Categoría 1",
            color: "seagreen"
        },
        {
            code: "CAT02",
            title: "Categoría 2",
            color: "lightgreen"
        },
        {
            code: "CAT03",
            title: "Categoría 3",
            color: "papayawhip"
        }
    ])

    // Categoria seleccionada
    const [selectedCategory, setSelectedCategory] = useState(null)

    const onDragOver = (e) => {
        e.preventDefault()
    }

    const onDragStart = (categoryCode) => {
        console.log('category that is being handled: ', categoryCode)
        setSelectedCategory(categoryCode)
    }

    const onDrop = (categoryCode) => {
        // Si se solto la categoría en el mismo lado
        if (!categoryCode || !selectedCategory || categoryCode === selectedCategory) return

        // Obtener el indice de la categoria seleccionada
        const selectedCategoryIndex = ArrayHelper.getIndexOfObjectArray(categoriesState, "code", selectedCategory)
        // Obtener el indice de la categoria que se cambió por la seleccionada 
        const switchedCategoryIndex = ArrayHelper.getIndexOfObjectArray(categoriesState, "code", categoryCode)
        
        if (selectedCategoryIndex === -1 || switchedCategoryIndex === -1) return
        
        const selectedCategoryObject = categoriesState[selectedCategoryIndex]
        const switchedCategoryObject = categoriesState[switchedCategoryIndex]

        // Hacer copia de las categorías actualizadas
        let updatedCategories = [...categoriesState]
        // Setear categoría seleccionada en su nuevo sitio:
        updatedCategories[selectedCategoryIndex] = switchedCategoryObject
        updatedCategories[switchedCategoryIndex] = selectedCategoryObject

        console.log({
            "categoriaInicial": categoriesState,
            "categoriaNueva": updatedCategories
        })

        setCategoriesState(updatedCategories)
    }

    return (
        <div className="DragAndDrop__container">
            <h1>Drag and Drop</h1>
            <hr />

            {/* Droppable container */}
            <div className="DragAndDrop__droppable-container">
                {/* Draggable content */}
                {
                    categoriesState.map(category =>
                        <div
                            key={category.code}
                            style={{ backgroundColor: category.color, marginRight: '1em', height: '6em' }}
                            className="DragAndDrop__draggable-item"
                            onDragStart={() => onDragStart(category.code)}
                            onDragOver={(e) => onDragOver(e)}
                            onDrop={() => onDrop(category.code, "complete")}
                            draggable
                        >
                            <p>{category.title}</p>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default DragAndDrop
