const getIndexOfObjectArray = (array, keyToSearch, valueToFind) => {
    if(!array || array.length === 0) return -1
    const keyArrays = array.map(item => item[keyToSearch])
    return keyArrays.indexOf(valueToFind)
}

const ArrayHelpers = {
    getIndexOfObjectArray
}

export default ArrayHelpers